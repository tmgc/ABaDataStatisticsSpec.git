//
//  ABaDataStatistics.h
//  ABaDataStatisticsSDK
//
//  Created by rxk on 2020/10/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ABaDataStatistics : NSObject
/**
 开启debug模式
 */
+ (void)startDebug;
/*!
 @method            openIntegralPage
 @param             userMobileStr 用户手机号.
 */
+ (void)openIntegralPageUserMobile:(NSString *__nullable)userMobileStr;

/*!
 @method            aba_startWithAppKey:appSecret:
 @abstract          初始化SDK.
 @param             appKey appKey.
 @param             appSecret appSecret.
 */
+ (void)aba_startWithAppKey:(NSString * __nullable)appKey
                  appSecret:(NSString * __nullable)appSecret;

/*!
 @method            aba_addPlayEventWithpProgramName:programID:userMobile:programLevel:programType:beginTime:endTime:
 @abstract          添加播放统计事件.
 @param             programName 节目名称.
 @param             programID 节目ID.
 @param             programLevel  新闻节目级别   0：其它  1：州级新闻节目 2：县级新闻节目.
 @param             userMobile 手机号.
 @param             programType 节目类型：2：电视播放  3：广播播放.
 @param             beginTime 开始时间.
 @param             endTime 结束时间.
 @param             userCode 用户唯一标识.
 */
+ (void)aba_addPlayEventWithpProgramName:(NSString *__nullable)programName
                               programID:(NSString *__nullable)programID
                              userMobile:(NSString *__nullable)userMobile
                            programLevel:(int)programLevel
                             programType:(int)programType
                               beginTime:(long)beginTime
                                 endTime:(long)endTime
                                userCode:(NSString *__nullable)userCode;

/*!
 @method            aba_addPlayEventWithpProgramName:programID:programType:beginTime:endTime:
 @abstract          添加播放统计事件.
 @param             programName 节目名称.
 @param             programID 节目ID.
 @param             programType 节目类型：2：电视播放  3：广播播放.
 @param             beginTime 开始时间.
 @param             endTime 结束时间.
 @param             userCode 用户唯一标识.
 */
+ (void)aba_addPlayEventWithpProgramName:(NSString *__nullable)programName
                               programID:(NSString *__nullable)programID
                             programType:(int)programType
                               beginTime:(long)beginTime
                                 endTime:(long)endTime
                                userCode:(NSString *__nullable)userCode DEPRECATED_MSG_ATTRIBUTE("0.0.4版本之后建议使用新方法，新方法中增加了两个属性programLevel 事件分级类型 0：其它  1：州级  2：县级.userMobile 手机号.");
@end

NS_ASSUME_NONNULL_END
