//
//  ABaDataStatWKwebViewController.h
//  ABaDataStatisticsSDK
//
//  Created by 鲜世杰 on 2020/12/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ABaDataStatWKwebViewController : UIViewController
//@property (nonatomic,copy)NSString *userMobile;

/**
 * 用于对Controller的参数传递，标准json格式等字符串 json字符串中需要包含一个iphone的key传递用户的手机号
 */
@property (nonatomic, copy) NSString *paramStr;


@end

NS_ASSUME_NONNULL_END
