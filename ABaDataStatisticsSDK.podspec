#
#  Be sure to run `pod spec lint TMSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "ABaDataStatisticsSDK"
  s.version      = "0.0.10"
  s.summary      = "阿坝数据统计SDK"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "renxukui" => "renxukui@360tianma.com" }
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/ABaDataStatisticsSpec.git", :tag => s.version }
  s.source_files = 'ABaFramework/ABaDataStatisticsSDK.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'ABaFramework/ABaDataStatisticsSDK.framework'
  s.requires_arc = true



    s.dependency "AFNetworking"
    s.dependency "BaiduMobStatCodeless"


end
